<html>
  <head>
    <%@ include file="/jspf/styles.jspf"%>
    <title>Order Confirmation</title>
  </head>

  <body>
    <%@ include file="/jspf/bar.jspf"%>
    <%@ include file="/jspf/connect.jspf"%>

    <%
    if(session.getAttribute("name") == null) {
      response.sendRedirect(request.getContextPath() + "/login.jsp");
    }
    %>

    <%
    String lastpage = (String) session.getAttribute("lastpage");
    if(lastpage != null && !lastpage.equals("buyshoppingcart")) {
      response.sendRedirect(request.getContextPath() + "/wronglastpage.jsp");
    }
    session.setAttribute("lastpage", "confirm");
    %>

    <div class="container">
    <%
    String result = request.getParameter("result");
    if(result != null && result.equals("failed")) {
      %>
      <h5>Something went wrong with your purchase</h5>
      <%
    }
    else {
    String card = request.getParameter("card");
    int cardNo;

    try {
      if ( (cardNo = Integer.parseInt(card)) <= 0) {
        card = null;
      }
    }
    catch (Exception e) {
      card = null;
    }
    finally {

    }

    if (card != null) {
      %>
      <%@ include file="/jspf/cart.jspf"%>
      <h5>Please confirm your purchase:</h5>

      <form action="Purchase" method="POST" id="confirm">
        <button class="btn waves-effect waves-light" type="submit" name="action">
          Confirm <i class="material-icons right">send</i>
        </button>
      </form>

      <%
    }
    else {
      %>
      <h5>Please input valid credit card number.</h5>
      </br>
      Please <a href="buyshoppingcart.jsp">Enter your credit card again</a>
      <%
    }
  }
    %>

    </div>

    <%@ include file="/jspf/close.jspf"%>
  </body>
</html>
