drop database cse135;
create database cse135;

CREATE TABLE states (
  id             SERIAL PRIMARY KEY,
  name           TEXT NOT NULL UNIQUE,
  abbreviation   TEXT NOT NULL UNIQUE
);

CREATE TABLE users (
  id             SERIAL PRIMARY KEY,
  name           TEXT NOT NULL UNIQUE CHECK (name <> ''),
  role           TEXT NOT NULL CHECK (role = 'owner' OR role = 'customer'),
  age            INTEGER NOT NULL CHECK (age > 0),
  state          INTEGER REFERENCES states (id) ON DELETE CASCADE
);

CREATE TABLE categories (
  id             SERIAL PRIMARY KEY,
  name           TEXT NOT NULL UNIQUE CHECK (name <> ''),
  description    TEXT NOT NULL
);

CREATE TABLE products (
  id             SERIAL PRIMARY KEY,
  name           TEXT NOT NULL CHECK (name <> ''),
  sku            TEXT NOT NULL UNIQUE,
  cid            INTEGER REFERENCES categories (id),
  price          INTEGER NOT NULL CHECK (price > 0)
);

CREATE TABLE carts (
  id             SERIAL PRIMARY KEY,
  uid            INTEGER REFERENCES users (id) ON DELETE CASCADE,
  pid            INTEGER REFERENCES products (id) ON DELETE CASCADE,
  quantity       INTEGER NOT NULL CHECK (quantity > 0),
  price          INTEGER NOT NULL CHECK (price > 0)
);

CREATE TABLE purchases (
  id             SERIAL PRIMARY KEY,
  uid            INTEGER REFERENCES users (id) ON DELETE CASCADE,
  pid            INTEGER REFERENCES products (id) ON DELETE CASCADE,
  quantity       INTEGER NOT NULL CHECK (quantity > 0),
  price          INTEGER NOT NULL CHECK (price > 0)
);

INSERT INTO states (name, abbreviation) VALUES
  ('Alabama',  'AL'),
  ('Alaska', 'AK'),
  ('Arizona', 'AZ'),
  ('Arkansas', 'AR'),
  ('California', 'CA'),
  ('Colorado', 'CO'),
  ('Connecticut', 'CT'),
  ('Delaware', 'DE'),
  ('Florida', 'FL'),
  ('Georgia', 'GA'),
  ('Hawaii', 'HI'),
  ('Idaho', 'ID'),
  ('Illinois', 'IL'),
  ('Indiana', 'IN'),
  ('Iowa', 'IA'),
  ('Kansas', 'KS'),
  ('Kentucky', 'KY'),
  ('Louisiana', 'LA'),
  ('Maine', 'ME'),
  ('Maryland', 'MD'),
  ('Massachusetts', 'MA'),
  ('Michigan', 'MI'),
  ('Minnesota', 'MN'),
  ('Mississippi', 'MS'),
  ('Missouri', 'MO'),
  ('Montana', 'MT'),
  ('Nebraska', 'NE'),
  ('Nevada', 'NV'),
  ('New Hampshire', 'NH'),
  ('New Jersey', 'NJ'),
  ('New Mexico', 'NM'),
  ('New York', 'NY'),
  ('North Carolina', 'NC'),
  ('North Dakota', 'ND'),
  ('Ohio', 'OH'),
  ('Oklahoma', 'OK'),
  ('Oregon', 'OR'),
  ('Pennsylvania', 'PA'),
  ('Rhode Island', 'RI'),
  ('South Carolina', 'SC'),
  ('South Dakota', 'SD'),
  ('Tennessee', 'TN'),
  ('Texas', 'TX'),
  ('Utah', 'UT'),
  ('Vermont', 'VT'),
  ('Virginia', 'VA'),
  ('Washington', 'WA'),
  ('West Virginia', 'WV'),
  ('Wisconsin', 'WI'),
  ('Wyoming', 'WY');
