<html>

<head>
  <%@ include file="/jspf/styles.jspf" %>
  <title>Buy Shopping Cart</title>
</head>

<body>
<%@ include file="/jspf/connect.jspf" %>
<%@ include file="/jspf/bar.jspf" %>

<%
if(session.getAttribute("name") == null) {
  response.sendRedirect(request.getContextPath() + "/login.jsp");
}
%>

<%
session.setAttribute("lastpage", "buyshoppingcart");
%>

<div class="container">
<%@ include file="/jspf/cart.jspf" %>

<%
if (total > 0) {
%>

<h3>Total price: $<%=total%></h3>
</br>
<h5>Pay Here:</h5>
<div class="row">
  <form class="col s12" action="confirm.jsp" method="GET">
    <div class="row">
      <div class="input-field col s6">
        Credit Card:
        <input name="card" type="text" class="validate" required autofocus>
      </div>
    </div>
    <button class="btn waves-effect waves-light" type="submit" name="action">
      Purchase <i class="material-icons right">send</i>
    </button>
  </form>
</div>

<%
}
else {
%>
<p class="error">Please add something to your cart first</p>
<%
}
%>

</div>

<%@ include file="/jspf/close.jspf" %>

</body>
</html>
