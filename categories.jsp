<html>
  <head>
    <%@ include file="jspf/styles.jspf" %>
    <title>Categories</title>
  </head>

  <body>
    <%@ include file="/jspf/connect.jspf"%>
    <%@ include file="/jspf/bar.jspf"%>

    <%
    if(session.getAttribute("name") == null) {
      response.sendRedirect(request.getContextPath() + "/login.jsp");
    }
    %>

    <%
    if(session.getAttribute("name") == null) {
      response.sendRedirect(request.getContextPath() + "/login.jsp");
    }
    %>

    <%
    session.setAttribute("lastpage", "categories");
    %>

    <div class="container">

      <%
      if(session.getAttribute("role").equals("customer")) {
      %>
      <h3>This page is available to owners only</h3>
      <%
      } else {
      %>

      <h3>Categories, Hello <% out.print(session.getAttribute("name")); %></h3>
      <div>
        <a href="categories.jsp">Categories</a>
        | <a href="products.jsp">Products</a>
        | <a href="productsbrowsing.jsp">Browse</a>
        | <a href="buyshoppingcart.jsp">Buy Sopping Cart</a>
      </div>

      <%
      String result = request.getParameter("result");
      if (result != null) {
        if (result.equals("ok")) {
        %>
          <h3 class="success" align="center">Success</h3>
        <%
        }
        else {
        %>
        <h3 class="success" align="center">Failure</h3>
        <%
        }
      }
      %>

      <%-- -------- SELECT Statement Code -------- --%>
      <%
      // Create the statement
      pstmt = conn.prepareStatement("SELECT * FROM categories");
      // Use the created statement to SELECT
      // the student attributes FROM the Student table.
      rs = pstmt.executeQuery();
      %>

      <!-- Add an HTML table header row to format the results -->
      <table border="1">
      <tr>
          <th>ID</th>
          <th>Category Name</th>
          <th>Description</th>
      </tr>

      <tr>
        <form action="CategoryForm" method="POST">
          <input type="hidden" name="action" value="insert"/>
          <th></th>
          <td><input value="" name="name" size="15"/></td>
          <td><input value="" name="description" size="20"/></td>
          <td><button class="waves-effect waves-light btn" type="submit">Insert</button></td>
        </form>
      </tr>

      <%-- -------- Iteration Code -------- --%>
      <%
      // Iterate over the ResultSet
      while (rs.next()) {
      %>

      <tr>
        <form action="CategoryForm" method="POST">
          <input type="hidden" name="action" value="update"/>
          <input type="hidden" name="id" value="<%=rs.getInt("id")%>"/>
          <%-- Get the id --%>
          <td>
            <%=rs.getInt("id")%>
          </td>
          <%-- Get the first name --%>
          <td>
            <input name="name" value="<%=rs.getString("name")%>" size="15"/>
          </td>
          <%-- Get the middle name --%>
          <td>
            <input name="description" value="<%=rs.getString("description")%>" size="50"/>
          </td>
          <%-- Button --%>
          <td>
            <button class="waves-effect waves-light btn" type="submit">Update</button>
          </td>
        </form>
        <form action="CategoryForm" method="POST">
          <input type="hidden" name="action" value="delete"/>
          <input type="hidden" value="<%=rs.getInt("id")%>" name="id"/>
          <%-- Button --%>
          <td>
            <button class="waves-effect waves-light btn" type="submit">Delete</button>
          </td>
        </form>
      </tr>
      <%
      }
      %>
    <%
    }
    %>

    <%@ include file="/jspf/close.jspf"%>
    <%@ include file="/jspf/scripts.jspf" %>
  </body>
</html>
