<html>
  <head>
    <%@ include file="jspf/styles.jspf" %>
    <title>Products</title>
  </head>

  <body>
    <%@ include file="/jspf/connect.jspf"%>
    <%@ include file="/jspf/bar.jspf"%>

    <%
    if(session.getAttribute("name") == null) {
      response.sendRedirect(request.getContextPath() + "/login.jsp");
    }
    %>

    <%
      session.setAttribute("lastpage", "products");
    %>

    <%@ page import="java.util.HashMap"%>
    <%@ page import="java.util.*"%>

    <div class="container">

      <%
      if(session.getAttribute("role").equals("customer")) {
      %>
      <h3>This page is available to owners only</h3>
      <%
      } else {
      %>

      <h3>Products, Hello <% out.print(session.getAttribute("name")); %></h3>
      <div>
        <a href="categories.jsp">Categories</a>
        | <a href="products.jsp">Products</a>
        | <a href="productsbrowsing.jsp">Browse</a>
        | <a href="buyshoppingcart.jsp">Buy Sopping Cart</a>
      </div>
    <%
    String result = request.getParameter("result");
    if (result != null) {
      if (result.equals("ok")) {
      %>
        <h3 class="success" align="center">Success</h3>
      <%
      }
      else {
      %>
      <h3 class="success" align="center">Failure</h3>
      <%
      }
    }
    %>

      <%-- -------- Get all categories from DB -------- --%>
      <%
      HashMap<Integer, String> categories = new HashMap<Integer, String>();
      // Create the statement
      pstmt = conn.prepareStatement("SELECT * FROM categories");
      // Use the created statement to SELECT
      // the student attributes FROM the Student table.
      rs = pstmt.executeQuery();

      while (rs.next()) {
        categories.put(rs.getInt("id"), rs.getString("name"));
      }

      String cat = request.getParameter("cat");
      String search = request.getParameter("search");
      %>

      <center>
        </br>
        Filter by Category:
        <a href="products.jsp">All</a>
        <%
        for(Integer key : categories.keySet()) {
          if(search == null) {
          %>
            <a href="products.jsp?cat=<%=key%>">| <%=categories.get(key)%></a>
          <%
          } else {
          %>
            <a href="products.jsp?cat=<%=key%>&search=<%=search%>">| <%=categories.get(key)%></a>
          <%
          }
        }
        %>
        </br>
        <form>
          <%
          if(cat != null) {
            %>
            <input type=hidden name="cat" value="<%=request.getParameter("cat")%>"/>
            <%
          }
          %>
          <input placeholder="Search" name="search" value="" />
        </form>
        </br>
        </br>

      </center>

      <div class="row">
        <form class="col s11" action="ProductForm" method="POST">
          <input type="hidden" name="action" value="insert"/>
          <input type="hidden" name="cat" value="<%=cat%>"/>
          <input type="hidden" name="search" value="<%=search%>"/>

          <div class="row">
            <div class="input-field col s1">
              <input value="" name="id" disabled/>
              <label for="id">ID</label>
            </div>
            <div class="input-field col s2">
              <input value="" name="name" type="text" required />
              <label for="name">Product Name</label>
            </div>
            <div class="input-field col s2">
              <input value="" name="sku" type="text" required/>
              <label for="sku">SKU</label>
            </div>
            <div class="input-field col s2">
              <select name="cid">
                <option value="" disabled selected>Choose a category</option>
                <%
                  for(Map.Entry<Integer, String> entry : categories.entrySet()) {
                    %>
                    <option value="<%out.print(entry.getKey().intValue());%>"><%out.print(entry.getValue());%></option>
                    <%
                  }
                %>
              </select>
              <label>Category</label>
            </div>
            <div class="input-field col s2">
              <input value="" name="price" type="text" required/>
              <label for="sku">Price</label>
            </div>
            <div class="input-field col s1">
              <button class="waves-effect waves-light btn" type="submit">Insert</button>
            </div>
          </div>
        </form>
      </div>

      <%-- -------- SELECT Statement Code -------- --%>
      <%

      if( search != null && cat != null) {
        pstmt = conn.prepareStatement("SELECT * FROM products WHERE cid = ? AND LOWER(name) LIKE LOWER(?)");
        pstmt.setInt(1, Integer.parseInt(cat));
        pstmt.setString(2, "%"+search+"%");
      }
      else if (search != null) {
        pstmt = conn.prepareStatement("SELECT * FROM products WHERE LOWER(name) LIKE LOWER(?)");
        pstmt.setString(1, "%"+search+"%");
      }
      else if(cat != null) {
        pstmt = conn.prepareStatement("SELECT * FROM products WHERE cid = ?");
        pstmt.setInt(1, Integer.parseInt(cat));
      }
      else {
        pstmt = conn.prepareStatement("SELECT * FROM products");
      }

      rs = pstmt.executeQuery();

      // Iterate over the ResultSet
      while (rs.next()) {
      %>

      <div class="row">
        <form class="col s11" action="ProductForm" method="POST">
          <input type="hidden" name="action" value="update"/>
          <input type="hidden" name="id" value="<%=rs.getInt("id")%>"/>
          <input type="hidden" name="cat" value="<%=cat%>"/>
          <input type="hidden" name="search" value="<%=search%>"/>

          <div class="row">
            <div class="input-field col s1">
              <input value="<%=rs.getInt("id")%>" name="id" disabled/>
            </div>
            <div class="input-field col s2">
              <input value="<%=rs.getString("name")%>" name="name" type="text" required />
              <label for="name">Product Name</label>
            </div>
            <div class="input-field col s2">
              <input value="<%=rs.getString("sku")%>" name="sku" type="text" required/>
              <label for="sku">SKU</label>
            </div>
            <div class="input-field col s2">
              <select name="cid" required>
                <option value="<%=rs.getInt("cid")%>" selected><%out.print(categories.get(rs.getInt("cid")));%></option>
                <%
                  for(Map.Entry<Integer, String> entry : categories.entrySet()) {
                    %>
                    <option value="<%out.print(entry.getKey().intValue());%>"><%out.print(entry.getValue());%></option>
                    <%
                  }
                %>
              </select>
              <label>Category</label>
            </div>
            <div class="input-field col s2">
              <input value="<%=rs.getString("price")%>" name="price" type="text" required/>
              <label for="sku">Price (in dollars)</label>
            </div>
            <div class="input-field col s1">
              <button class="waves-effect waves-light btn" type="submit">Update</button>
            </div>
          </div>
        </form>
        <form class="col s1" action="ProductForm" method="POST">
          <input type="hidden" name="action" value="delete"/>
          <input type="hidden" value="<%=rs.getInt("id")%>" name="id"/>
          <input type="hidden" name="cat" value="<%=cat%>"/>
          <input type="hidden" name="search" value="<%=search%>"/>
          <div class="input-field col s1">
            <button class="waves-effect waves-light btn" type="submit">Delete</button>
          </div>
        </form>
      </div>
      <%
      }
      %>
    <%
    }
    %>

    <%@ include file="/jspf/close.jspf"%>
    <%@ include file="/jspf/scripts.jspf" %>
  </body>
</html>
