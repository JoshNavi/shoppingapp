<html>
  <head>
    <%@ include file="/jspf/styles.jspf"%>
    <title>Shopping Cart</title>
  </head>

  <body>
    <%@ include file="/jspf/connect.jspf"%>
    <%@ include file="/jspf/bar.jspf"%>

    <%
    if(session.getAttribute("name") == null) {
      response.sendRedirect(request.getContextPath() + "/login.jsp");
    }
    %>

    <%
    String lastpage = (String) session.getAttribute("lastpage");
    if(lastpage != null && !lastpage.equals("productsbrowsing")) {
      response.sendRedirect(request.getContextPath() + "/wronglastpage.jsp");
    }
    session.setAttribute("lastpage", "productsorder");
    %>

    <%
    String id = request.getParameter("id");
    if(id == null) {
      response.sendRedirect(request.getContextPath() + "/productsbrowsing.jsp");
    }

    String result = request.getParameter("result");
    if (result != null) {
      if (result.equals("ok")) {
      %>
        <h3 class="success" align="center">Your have successfully added to your cart</h3>
      <%
      }
      else {
      %>
        <h5>Unable to add to cart</h5>
      <%
      }
    }
    %>

    <%-- INSERT FORM FOR QUANTITY CONFIRMATION --%>
    <div class="container">
      <h3>Item to add to cart:</h3>

    <%
    pstmt = conn.prepareStatement("SELECT * FROM products WHERE id = ?");
    pstmt.setInt(1, Integer.parseInt(id));
    rs = pstmt.executeQuery();

    while (rs.next()) {
    %>
      <div class="row">
        <form class="col s10" action="ProductOrderForm" method="POST">
          <input type="hidden" name="id" value="<%=rs.getInt("id")%>"/>
          <input type="hidden" name="price" value="<%=rs.getInt("price")%>"/>
          <input size="20" disabled name="name" value="<%=rs.getString("name")%>"/>
          <input size="20" disabled name="p" value="<%=rs.getInt("price")%>"/>
          <input value="1" name="quantity"/>
          <input class="btn btn-default btn-block" type="submit" value="insert"/></td>
        </form>
      </div>

    <%-- <div class="row">
      <div class="col s11">
        <div class="row">
          <div class="col s1">
            <div name="id"><%=rs.getInt("id")%></div>
          </div>
          <div class="col s3">
            <a href="productsorder.jsp?id=<%=rs.getInt("id")%>" name="name"><%=rs.getString("name")%></a>
          </div>
          <div class="col s3">
            <div name="sku"><%=rs.getString("sku")%></div>
          </div>
          <div class="col s2">
            <div name="cid"><%=categories.get(rs.getInt("cid"))%></div>
          </div>
          <div class="col s2">
            <div name="Price">$<%=rs.getString("price")%></div>
          </div>
        </div>
      </div>
    </div> --%>
    <%
    }
    %>
    <%@ include file="/jspf/cart.jspf"%>
  </div>
  <%@ include file="/jspf/close.jspf"%>
</body>
</html>
