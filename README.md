# Shopping App

#### Assumptions and Design Choices
Our user's name, while it must be unique is case sensitive, this means that for example there can be a user `Josh` and a user `josh` and these will be considered valid as two different users. Because of this the login credentials are also case sensitive.

Anytime someone tries to access a page that is not either the index (signup) page or the login page the user is redirected to the login page. This is handled by storing user data in a session attribute on successful login and checking for the user upon arriving at any page. The same information is used to show the user's name in the `Hello, user` display.

Any owner can update or delete any category as long as it does not violate a constraint. If an owner tries to perform a delete that would violate a constraint the user will see a Failure message at the top of the page. This is handled in the not preferred method of catching the exception after issuing the query to the database and having the database check the constraints.

The shopping cart is persistent and does not allow updates or deletes additionally the price of an item in the cart will reflect the price of the item when it was added to the cart regardless of whether or not the product has been changed elsewhere. However the name of the item in the cart will reflect any changes made to the items name.
