<html>
  <head>
    <%@ include file="jspf/styles.jspf" %>
    <title>Shopping App</title>
  </head>

  <body>
    <%@ include file="/jspf/connect.jspf"%>
    <%@ include file="/jspf/bar.jspf"%>

    <%
    if(session.getAttribute("name") == null) {
      response.sendRedirect(request.getContextPath() + "/login.jsp");
    }
    %>

    <%
    session.setAttribute("lastpage", "homepage");
    %>

    <div class="container">
    <h3>Homepage, Hello <% out.print(session.getAttribute("name")); %></h3>
    <div>
      <a href="categories.jsp">Categories</a>
      | <a href="products.jsp">Products</a>
      | <a href="productsbrowsing.jsp">Browse</a>
      | <a href="buyshoppingcart.jsp">Buy Sopping Cart</a>
    </div>

    <%@ include file="/jspf/close.jspf"%>
    <%@ include file="/jspf/scripts.jspf" %>
  </body>
</html>
