<%@ include file="/jspf/connect.jspf" %>
<%
String result = "failed";

try {
  int uid = (int) session.getAttribute("id");
  
  conn.setAutoCommit(false);
  pstmt = conn.prepareStatement(
    "INSERT INTO purchases (uid, pid, quantity, price) " +
    "(SELECT uid, pid, quantity, price " +
    "FROM carts " +
    "WHERE uid = ?)"
  );
  pstmt.setInt(1, uid);
  pstmt.executeUpdate();

  pstmt = conn.prepareStatement(
    "DELETE " +
    "FROM carts " +
    "WHERE uid = ?"
  );
  pstmt.setInt(1, uid);
  pstmt.executeUpdate();
  conn.commit();
  conn.setAutoCommit(true);

  result = "ok";
}
catch (SQLException e) {
  System.err.println("SQL Exception");
  e.printStackTrace();
}
catch (Exception e) {
  System.err.println("Other Exception");
  e.printStackTrace();
}

if (result.equals("ok")) {
  response.sendRedirect(request.getContextPath() + "/productsbrowsing.jsp");
}
else {
  response.sendRedirect(request.getContextPath() + "/confirm.jsp?result=" + result);
}

%>
<%@ include file="/jspf/close.jspf" %>
