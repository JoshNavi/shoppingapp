<%@ include file="/jspf/connect.jspf" %>
<%
  String action = request.getParameter("action");
  String id = request.getParameter("id");
  String name = request.getParameter("name");
  String sku = request.getParameter("sku");
  String cid = request.getParameter("cid");
  String price = request.getParameter("price");
  String cat = request.getParameter("cat");
  String search = request.getParameter("search");
  String result = "failed";

  try {
    if (action.equals("insert")) {
      pstmt = conn.prepareStatement(
        "INSERT INTO products (name, sku, cid, price) "+
        "VALUES (?, ?, ?, ?)"
      );

      pstmt.setString(1, name);
      pstmt.setString(2, sku);
      pstmt.setInt(3, Integer.parseInt(cid));
      pstmt.setInt(4, Integer.parseInt(price));
    }
    if (action.equals("update")) {
      pstmt = conn.prepareStatement(
        "UPDATE products SET name = ?, sku = ?, cid = ?, price = ? "+
        "WHERE id=?"
      );

      pstmt.setString(1, name);
      pstmt.setString(2, sku);
      pstmt.setInt(3, Integer.parseInt(cid));
      pstmt.setInt(4, Integer.parseInt(price));
      pstmt.setInt(5, Integer.parseInt(id));
    }
    if (action.equals("delete")) {
        pstmt = conn.prepareStatement("DELETE FROM products WHERE id = ?");
        pstmt.setInt(1, Integer.parseInt(id));
    }
    int rowCount = pstmt.executeUpdate();
    result = "ok";
  }
  catch (SQLException e) {
    System.err.println("SQLException");
    e.printStackTrace();
  }
  catch (Exception e) {
    System.err.println("Other Exception");
    e.printStackTrace();
  }

  String params = "";
  if(cat != null && search != null) {
    params += "&cat=" + cat + "&search=" + search;
  }
  else if(cat != null) {
    params += "&cat=" + cat;
  }
  else if(search != null) {
    params += "&search=" + search;
  }
  response.sendRedirect(request.getContextPath() + "/products.jsp?result=" + result + params);
%>
<%@ include file="/jspf/close.jspf" %>
