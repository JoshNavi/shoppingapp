<%@ include file="/jspf/connect.jspf" %>
<%
  String action = request.getParameter("action");
  String id = request.getParameter("id");
  String name = request.getParameter("name");
  String description = request.getParameter("description");

  String result = "failed";
  try {
    if (action.equals("insert")) {
      pstmt = conn.prepareStatement(
        "INSERT INTO categories (name, description) " +
        "VALUES (?, ?)"
      );

      pstmt.setString(1, name);
      pstmt.setString(2, description);
    }
    if (action.equals("update")) {
  	  pstmt = conn.prepareStatement(
        "UPDATE categories " +
        "SET name = ?, description = ? " +
        "WHERE id=?"
      );

      pstmt.setString(1, name);
      pstmt.setString(2, description);
      pstmt.setInt(3, Integer.parseInt(id));
    }
    if (action.equals("delete")) {
    	pstmt = conn.prepareStatement("DELETE FROM categories WHERE id = ?");
      pstmt.setInt(1, Integer.parseInt(id));
    }

    int rowCount = pstmt.executeUpdate();
    result = "ok";
  }
  catch (SQLException e) {
    System.err.println("SQLException");
    e.printStackTrace();
  }
  catch (Exception e) {
    System.err.println("Other Exception");
    e.printStackTrace();
  }

  response.sendRedirect(request.getContextPath() + "/categories.jsp?result=" + result);
%>
<%@ include file="/jspf/close.jspf" %>
