<%@ include file="/jspf/connect.jspf" %>

<%
  String name = request.getParameter("name");
  String role = request.getParameter("role");
  String state = request.getParameter("state");
  String age = request.getParameter("age");

  String result = "failed";
  // action is expected to be non-null
  try {
    // add user to database
    pstmt = conn.prepareStatement(
      "INSERT INTO users (name, role, age, state)" +
      "VALUES (?, ?, ?, ?)"
    );
    pstmt.setString(1, name);
    pstmt.setString(2, role);
    pstmt.setInt(3, Integer.parseInt(age));
    pstmt.setInt(4, Integer.parseInt(state));
    int rowCount = pstmt.executeUpdate();
    result = "ok";
  }
  catch (SQLException e) {
    System.err.println("SQLException");
    e.printStackTrace();
  }
  catch (Exception e) {
    System.err.println("Other Exception");
    e.printStackTrace();
  }

  response.sendRedirect(request.getContextPath() + "/index.jsp?result=" + result);
%>

<%@ include file="/jspf/close.jspf" %>
