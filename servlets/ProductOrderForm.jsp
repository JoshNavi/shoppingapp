<%@ include file="/jspf/connect.jspf" %>
<%
  int uid = (Integer)session.getAttribute("id");
  String pid = request.getParameter("id");
  String name = request.getParameter("name");
  String price = request.getParameter("price");
  String quantity = request.getParameter("quantity");
  String result = "failed";

  try {
      pstmt = conn.prepareStatement(
        "INSERT INTO carts (uid, pid, price, quantity) " +
        "VALUES (?, ?, ?, ?)"
      )
      ;
      pstmt.setInt(1, uid);
      pstmt.setInt(2, Integer.parseInt(pid));
      pstmt.setInt(3, Integer.parseInt(price));
      pstmt.setInt(4, Integer.parseInt(quantity));

      int rowCount = pstmt.executeUpdate();
      result = "ok";
  }
  catch (SQLException e) {
      System.err.println("SQL Exception");
      e.printStackTrace();
  }
  catch (Exception e) {
      System.err.println("Other Exception");
      e.printStackTrace();
  }
  // pass inserted info for confirmation
  if (result.equals("ok")) {
      response.sendRedirect(request.getContextPath() + "/productsbrowsing.jsp?result=" + result);
  }
  else {
      response.sendRedirect(request.getContextPath() + "/productsorder.jsp?id=" + pid + "&result=" + result);
  }
%>
<%@ include file="/jspf/close.jspf" %>
