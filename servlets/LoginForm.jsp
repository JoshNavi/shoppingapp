<%@ include file="/jspf/connect.jspf" %>
<%@ page import="java.io.*"%>

<%
  String name = request.getParameter("name");

  String result = "failed";
  // action is expected to be non-null
  try {
    // add user to database
    pstmt = conn.prepareStatement(
      "SELECT * " +
      "FROM users " +
      "WHERE name = ?"
    );

    pstmt.setString(1, name);
    rs = pstmt.executeQuery();
    if(rs.next()) {
      if(rs.getString("name").equals(name)) {
        session.setAttribute("name", name);
        session.setAttribute("id", rs.getInt("id"));
        session.setAttribute("role", rs.getString("role"));
        result = "ok";
      }
    }
  }
  catch (SQLException e) {
    System.err.println("SQLException");
    e.printStackTrace();
  }
  catch (Exception e) {
    System.err.println("Other Exception");
    e.printStackTrace();
  }

  if(result.equals("ok")) {
    response.sendRedirect(request.getContextPath() + "/homepage.jsp");
  }
  else {
    response.sendRedirect(request.getContextPath() + "/login.jsp?result=" + result);
  }
%>

<%@ include file="/jspf/close.jspf" %>
