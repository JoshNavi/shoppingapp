<html>
  <head>
    <%@ include file="/jspf/styles.jspf" %>
    <title>Login</title>
  </head>

  <body>
    <%@ include file="/jspf/connect.jspf"%>
    <%@ include file="/jspf/bar.jspf"%>

    <%
      session.setAttribute("lastpage", "login");
    %>

    <div class="container">
      <h3>Log In</h3>

      <div class="row">
        <form class="col s12" action="LoginForm" method="POST">
          <div class="row">
            <div class="input-field col s6">
              <input name="name" id="name" type="text" class="validate" required autofocus>
              <label for="name">Name</label>
            </div>
          </div>
          <button class="btn waves-effect waves-light" type="submit" name="action">
            Submit <i class="material-icons right">send</i>
          </button>
        </form>
      </div>

      <%
      // sign up failed
      String result = request.getParameter("result");
      if (result != null) {
        if (result.equals("ok")) {
        %>
          <h3 class="success" align="center">Your have successfully logged in!</h3>
        <%
        }
        else {
        %>
          <h5>The provided name <% out.print(result); %> is not known</h5>
        <%
        }
      }
      %>

    </div>

    <%@ include file="/jspf/close.jspf"%>
    <%@ include file="/jspf/scripts.jspf" %>
  </body>
</html>
