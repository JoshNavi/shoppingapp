<html>
  <head>
    <%@ include file="jspf/styles.jspf" %>
    <title>Products</title>
  </head>

  <body>
    <%@ include file="/jspf/connect.jspf"%>
    <%@ include file="/jspf/bar.jspf"%>

    <%
    if(session.getAttribute("name") == null) {
      response.sendRedirect(request.getContextPath() + "/login.jsp");
    }
    %>

    <%
    session.setAttribute("lastpage", "productsbrowsing");
    %>

    <%@ page import="java.util.HashMap"%>
    <%@ page import="java.util.*"%>

    <div class="container">
      <h3>Browsing, Hello <% out.print(session.getAttribute("name")); %></h3>

    <%
    String result = request.getParameter("result");
    if (result != null) {
      if (result.equals("ok")) {
      %>
        <h3 class="success" align="center">Success</h3>
      <%
      }
      else {
      %>
      <h3 class="success" align="center">Failure</h3>
      <%
      }
    }
    %>

      <%-- -------- Get all categories from DB -------- --%>
      <%
      HashMap<Integer, String> categories = new HashMap<Integer, String>();
      // Create the statement
      pstmt = conn.prepareStatement("SELECT * FROM categories");
      // Use the created statement to SELECT
      // the student attributes FROM the Student table.
      rs = pstmt.executeQuery();

      while (rs.next()) {
        categories.put(rs.getInt("id"), rs.getString("name"));
      }

      String cat = request.getParameter("cat");
      String search = request.getParameter("search");
      %>

      <center>
        </br>
        Filter by Category:
        <a href="productsbrowsing.jsp">All</a>
        <%
        for(Integer key : categories.keySet()) {
          if(search == null) {
          %>
            <a href="productsbrowsing.jsp?cat=<%=key%>">| <%=categories.get(key)%></a>
          <%
          } else {
          %>
            <a href="productsbrowsing.jsp?cat=<%=key%>&search=<%=search%>">| <%=categories.get(key)%></a>
          <%
          }
        }
        %>
        </br>
        <form>
          <%
          if(cat != null) {
            %>
            <input type=hidden name="cat" value="<%=request.getParameter("cat")%>"/>
            <%
          }
          %>
          <input placeholder="Search" name="search" value="" />
        </form>
        </br>
        </br>

      </center>

      <div class="row">
        <div class="col s11">
          <div class="row">
            <div class="col s1">
              <div name="id">ID</div>
            </div>
            <div class="col s3">
              <div name="name">Product Name</div>
            </div>
            <div class="col s3">
              <div name="sku">SKU</div>
            </div>
            <div class="col s2">
              <div name="cid">Category</div>
            </div>
            <div class="col s2">
              <div name="Price">Price</div>
            </div>
          </div>
        </div>
      </div>

      <%-- -------- SELECT Statement Code -------- --%>
      <%
      // String cat = request.getParameter("cat");
      // String search = request.getParameter("search");

      if( search != null && cat != null) {
        pstmt = conn.prepareStatement("SELECT * FROM products WHERE cid = ? AND LOWER(name) LIKE LOWER(?)");
        pstmt.setInt(1, Integer.parseInt(cat));
        pstmt.setString(2, "%"+search+"%");
      }
      else if (search != null) {
        pstmt = conn.prepareStatement("SELECT * FROM products WHERE LOWER(name) LIKE LOWER(?)");
        pstmt.setString(1, "%"+search+"%");
      }
      else if(cat != null) {
        pstmt = conn.prepareStatement("SELECT * FROM products WHERE cid = ?");
        pstmt.setInt(1, Integer.parseInt(cat));
      }
      else {
        pstmt = conn.prepareStatement("SELECT * FROM products");
      }

      rs = pstmt.executeQuery();

      // Iterate over the ResultSet
      while (rs.next()) {
      %>

      <div class="row">
        <div class="col s11">
          <div class="row">
            <div class="col s1">
              <div name="id"><%=rs.getInt("id")%></div>
            </div>
            <div class="col s3">
              <a href="productsorder.jsp?id=<%=rs.getInt("id")%>" name="name"><%=rs.getString("name")%></a>
            </div>
            <div class="col s3">
              <div name="sku"><%=rs.getString("sku")%></div>
            </div>
            <div class="col s2">
              <div name="cid"><%=categories.get(rs.getInt("cid"))%></div>
            </div>
            <div class="col s2">
              <div name="Price">$<%=rs.getString("price")%></div>
            </div>
          </div>
        </div>
      </div>
      <%
      }
      %>

    <%@ include file="/jspf/close.jspf"%>
    <%@ include file="/jspf/scripts.jspf" %>
  </body>
</html>
