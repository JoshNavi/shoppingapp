<html>
  <head>
    <%@ include file="/jspf/styles.jspf" %>
    <title>Sign Up</title>
  </head>

  <body>
    <%@ include file="/jspf/connect.jspf"%>
    <%@ include file="/jspf/bar.jspf"%>

    <%
      session.setAttribute("lastpage", "index");
    %>

    <div class="container">
      <h3>Please sign up to use the app</h3>

      <div class="row">
        <form class="col s12" action="SignUpForm" method="POST">
          <div class="row">
            <div class="input-field col s6">
              <input name="name" id="name" type="text" class="validate" required autofocus>
              <label for="name">Name</label>
            </div>
            <div class="input-field col s6">
              <input name="age" id="age" type="text" class="validate" required>
              <label for="age">Age</label>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s6">
              <select name="role" required>
                <option value="" disabled selected>Choose your role</option>
                <option value="owner">Owner</option>
                <option value="customer">Customer</option>
              </select>
              <label for="role">Role</label>
            </div>
            <div class="input-field col s6">
              <select name="state" required>
                <option value="" disabled selected>Choose your State</option>
                <%
                pstmt = conn.prepareStatement("SELECT * FROM states");
                rs = pstmt.executeQuery();
                while( rs.next() ){
                %>
                  <option value="<%=rs.getInt("id")%>"> <%=rs.getString("name")%> </option>
                <%
                }
                %>
              </select>
              <label for="state">State</label>
            </div>
          </div>
          <button class="btn waves-effect waves-light" type="submit" name="action">
            Submit <i class="material-icons right">send</i>
          </button>
        </form>
      </div>

      <%
      // sign up failed
      String result = request.getParameter("result");
      if (result != null) {
        if (result.equals("ok")) {
        %>
          <h3 class="success" align="center">Your have successfully<br>signed up!</h3>
        <%
        }
        else {
        %>
          <h3 class="error" align="center">Your signup failed!</h3>
        <%
        }
      }
      %>

      <h5>
        <a href="login.jsp">Already have an account?Login here!</a>
      </h5>
    </div>

    <%@ include file="/jspf/close.jspf"%>
    <%@ include file="/jspf/scripts.jspf" %>
  </body>
</html>
